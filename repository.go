package storage

import (
	"context"

	"gitlab.com/gorib/storage/sql"
)

type Repository interface {
	Begin(ctx context.Context) (childCtx context.Context, commit func() error, rollback func() error, err error)
}

type SqlRepository[Model any] interface {
	Repository
	Get(ctx context.Context, builder sql.Builder) (Model, error)
	Select(ctx context.Context, builder sql.Builder) ([]*Model, error)
	Delete(ctx context.Context, builder sql.Builder) error
	Count(ctx context.Context, query sql.Builder) (int, error)
}
