package sql

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/gorib/ptr"
)

var (
	dialects = make(map[string]func(*builder) (string, *[]any, error))
)

func init() {
	dialects["postgres"] = dialectPsql()
	dialects["sqlite3"] = dialectSqlite()
}

func RegisterAlias(dialect, alias string) error {
	d := dialects[dialect]
	if d == nil {
		return fmt.Errorf("dialect not found: '%s'", dialect)
	}
	dialects[alias] = d
	return nil
}

func dialectPsql() func(*builder) (string, *[]any, error) {
	exprBuilderHandler := func(field string, value any, operation string, params *[]any) (string, *[]any, error) {
		operationsList := map[string]string{
			"eq":      "=",
			"lt":      "<",
			"le":      "<=",
			"lte":     "<=",
			"gt":      ">",
			"ge":      ">=",
			"gte":     ">=",
			"ne":      "!=",
			"neq":     "!=",
			"nin":     "!=",
			"json_eq": "?",
			"json_in": "@>",
			"between": "between",
			"like":    "like",
		}

		if op, ok := operationsList[operation]; ok {
			if v, ok := value.(SqlExpr); ok {
				return fmt.Sprintf("%s %s %s", field, op, v), params, nil
			}
			v := reflect.ValueOf(value)
			if value == nil || v.Kind() == reflect.Ptr && v.IsNil() {
				op = map[string]string{
					"=":  "is null",
					"!=": "is not null",
				}[op]
				return fmt.Sprintf("%s %s", field, op), params, nil
			}
			if v.Kind() == reflect.Slice {
				op = map[string]string{
					"=":       "in",
					"!=":      "not in",
					"?":       "?|",
					"between": "between",
				}[op]
				list := make([]string, 0, v.Len())
				for i := 0; i < v.Len(); i++ {
					*params = append(*params, v.Index(i).Interface())
					list = append(list, fmt.Sprintf("$%d", len(*params)))
				}
				var in string
				if len(list) == 0 {
					return "0=1", params, nil
				} else if op == "?|" {
					in = fmt.Sprintf("array[%s]", strings.Join(list, ","))
				} else if op == "between" {
					if v.Len() != 2 {
						return "", nil, fmt.Errorf("wrong arguments count for operation 'between': %v", value)
					}
					in = strings.Join(list, " and ")
				} else {
					in = fmt.Sprintf("(%s)", strings.Join(list, ","))
				}
				return fmt.Sprintf("%s %s %s", field, op, in), params, nil
			}
			*params = append(*params, value)
			return fmt.Sprintf("%s %s $%d", field, op, len(*params)), params, nil
		}
		return "", nil, fmt.Errorf("unsupported sql operation: '%s'", operation)
	}

	return func(b *builder) (string, *[]any, error) {
		query := ""
		if b.params == nil {
			b.params = ptr.Pointer([]any{})
		}

		switch b.queryMode {
		case modeSelect:
			if len(b.fields) == 0 {
				b.fields = append(b.fields, "*")
			}
			query = fmt.Sprintf("select %s from", strings.Join(b.fields, ","))
		case modeDelete:
			query = "delete from"
		case modeInsert:
			query = "insert into"
		case modeUpdate:
			query = "update"
		default:
			return "", nil, fmt.Errorf("unsupported mode '%d'", b.queryMode)
		}

		if b.table == "" && b.subSelect == nil {
			return "", nil, fmt.Errorf("no table specified")
		}
		if b.table != "" {
			query = fmt.Sprintf("%s %s", query, b.table)
		}
		if b.queryMode == modeSelect && b.subSelect != nil {
			subquery, params, err := b.subSelect.Build()
			if err != nil {
				return "", nil, err
			}
			query = fmt.Sprintf("%s (%s) s", query, subquery)
			*b.params = append(*b.params, *params...)
		}

		switch b.queryMode {
		case modeInsert:
			query = fmt.Sprintf("%s (%s)", query, strings.Join(b.fields, ", "))
			var inserts []string
			if b.namedMode {
				for _, field := range b.fields {
					inserts = append(inserts, ":"+field)
				}
				b.params = ptr.Pointer(b.inserts[0])
				query = fmt.Sprintf("%s values (%s)", query, strings.Join(inserts, ", "))
			} else if b.subSelect != nil {
				subquery, params, err := b.subSelect.Build()
				if err != nil {
					return "", nil, err
				}
				query = fmt.Sprintf("%s %s", query, subquery)
				*b.params = append(*b.params, *params...)
			} else {
				for _, value := range b.inserts {
					if len(value) != len(b.fields) {
						return "", nil, fmt.Errorf("wrong insert value")
					}
					var line []string
					for idx := range value {
						if r, ok := value[idx].(SqlExpr); ok {
							line = append(line, r.String())
						} else {
							*b.params = append(*b.params, value[idx])
							line = append(line, fmt.Sprintf("$%d", len(*b.params)))
						}
					}
					inserts = append(inserts, strings.Join(line, ", "))
				}
				query = fmt.Sprintf("%s values (%s)", query, strings.Join(inserts, "), ("))
			}
		case modeUpdate:
			var updates []string
			for field, value := range b.updates {
				if r, ok := value.(SqlExpr); ok {
					updates = append(updates, fmt.Sprintf("%s=%s", field, r.String()))
				} else {
					*b.params = append(*b.params, value)
					updates = append(updates, fmt.Sprintf("%s=$%d", field, len(*b.params)))
				}
			}
			query = fmt.Sprintf("%s set %s", query, strings.Join(updates, ", "))
		default:
		}

		if b.where != nil && b.queryMode != modeInsert {
			where, _, err := NewExpressionBuilder(b.where, exprBuilderHandler).Build(b.params)
			if err != nil {
				return "", nil, err
			}
			if where != "" {
				query = fmt.Sprintf("%s where %s", query, where)
			}
		}

		if b.window != "" {
			query = fmt.Sprintf("%s window %s", query, b.window)
		}

		if b.queryMode == modeSelect && len(b.group) > 0 {
			query = fmt.Sprintf("%s group by %s", query, strings.Join(b.group, ","))
		}

		if b.queryMode == modeSelect {
			if len(b.sort) == 0 {
				b.sort = []string{"id asc"}
			}
			query = fmt.Sprintf("%s order by %s", query, strings.Join(b.sort, ","))
		}

		if b.page != 0 {
			offset := b.page
			if b.count != 0 {
				offset *= b.count
			}
			query = fmt.Sprintf("%s offset %d", query, offset)
		}
		if b.count != 0 {
			query = fmt.Sprintf("%s limit %d", query, b.count)
		}

		if b.queryMode != modeSelect && b.conflictKey != "" {
			if len(b.conflictFields) == 0 {
				query = fmt.Sprintf("%s on conflict (%s) do nothing", query, b.conflictKey)
			} else {
				var conflicts []string
				for _, field := range b.conflictFields {
					conflicts = append(conflicts, fmt.Sprintf("%s=excluded.%s", field, field))
				}
				query = fmt.Sprintf("%s on conflict (%s) do update set %s", query, b.conflictKey, strings.Join(conflicts, ", "))
			}
		}

		if len(b.returning) > 0 && b.queryMode != modeSelect {
			query = fmt.Sprintf("%s returning %s", query, strings.Join(b.returning, ","))
		}

		return query, b.params, nil
	}
}

func dialectSqlite() func(*builder) (string, *[]any, error) {
	exprBuilderHandler := func(field string, value any, operation string, params *[]any) (string, *[]any, error) {
		operationsList := map[string]string{
			"eq":      "=",
			"lt":      "<",
			"le":      "<=",
			"lte":     "<=",
			"gt":      ">",
			"ge":      ">=",
			"gte":     ">=",
			"ne":      "!=",
			"neq":     "!=",
			"nin":     "!=",
			"between": "between",
			"like":    "like",
		}

		if op, ok := operationsList[operation]; ok {
			if v, ok := value.(SqlExpr); ok {
				return fmt.Sprintf("%s %s %s", field, op, v), params, nil
			}
			v := reflect.ValueOf(value)
			if value == nil || v.Kind() == reflect.Ptr && v.IsNil() {
				op = map[string]string{
					"=":  "is null",
					"!=": "is not null",
				}[op]
				return fmt.Sprintf("%s %s", field, op), params, nil
			}
			if v.Kind() == reflect.Slice {
				op = map[string]string{
					"=":       "in",
					"!=":      "not in",
					"between": "between",
				}[op]
				list := make([]string, 0, v.Len())
				for i := 0; i < v.Len(); i++ {
					*params = append(*params, v.Index(i).Interface())
					list = append(list, "?")
				}
				var in string
				if len(list) == 0 {
					return "0=1", params, nil
				} else if op == "between" {
					if v.Len() != 2 {
						return "", nil, fmt.Errorf("wrong arguments count for operation 'between': %v", value)
					}
					in = strings.Join(list, " and ")
				} else {
					in = fmt.Sprintf("(%s)", strings.Join(list, ","))
				}
				return fmt.Sprintf("%s %s %s", field, op, in), params, nil
			}
			*params = append(*params, value)
			return fmt.Sprintf("%s %s $%d", field, op, len(*params)), params, nil
		}
		return "", nil, fmt.Errorf("unsupported sql operation: '%s'", operation)
	}

	return func(b *builder) (string, *[]any, error) {
		query := ""
		if b.params == nil {
			b.params = ptr.Pointer([]any{})
		}

		switch b.queryMode {
		case modeSelect:
			if len(b.fields) == 0 {
				b.fields = append(b.fields, "*")
			}
			query = fmt.Sprintf("select %s from", strings.Join(b.fields, ","))
		case modeDelete:
			query = "delete from"
		case modeInsert:
			query = "insert into"
		case modeUpdate:
			query = "update"
		default:
			return "", nil, fmt.Errorf("unsupported mode '%d'", b.queryMode)
		}

		if b.table == "" && b.subSelect == nil {
			return "", nil, fmt.Errorf("no table specified")
		}
		if b.table != "" {
			query = fmt.Sprintf("%s %s", query, b.table)
		}
		if b.queryMode == modeSelect && b.subSelect != nil {
			subquery, params, err := b.subSelect.Build()
			if err != nil {
				return "", nil, err
			}
			query = fmt.Sprintf("%s (%s) s", query, subquery)
			*b.params = append(*b.params, *params...)
		}

		switch b.queryMode {
		case modeInsert:
			query = fmt.Sprintf("%s (%s)", query, strings.Join(b.fields, ", "))
			var inserts []string
			if b.namedMode {
				for _, field := range b.fields {
					inserts = append(inserts, ":"+field)
				}
				b.params = ptr.Pointer(b.inserts[0])
				query = fmt.Sprintf("%s values (%s)", query, strings.Join(inserts, ", "))
			} else if b.subSelect != nil {
				subquery, params, err := b.subSelect.Build()
				if err != nil {
					return "", nil, err
				}
				query = fmt.Sprintf("%s %s", query, subquery)
				*b.params = append(*b.params, *params...)
			} else {
				for _, value := range b.inserts {
					if len(value) != len(b.fields) {
						return "", nil, fmt.Errorf("wrong insert value")
					}
					var line []string
					for idx := range value {
						if r, ok := value[idx].(SqlExpr); ok {
							line = append(line, r.String())
						} else {
							*b.params = append(*b.params, value[idx])
							line = append(line, "?")
						}
					}
					inserts = append(inserts, strings.Join(line, ", "))
				}
				query = fmt.Sprintf("%s values (%s)", query, strings.Join(inserts, "), ("))
			}
		case modeUpdate:
			var updates []string
			for field, value := range b.updates {
				if r, ok := value.(SqlExpr); ok {
					updates = append(updates, fmt.Sprintf("%s=%s", field, r.String()))
				} else {
					*b.params = append(*b.params, value)
					updates = append(updates, fmt.Sprintf("%s=?", field))
				}
			}
			query = fmt.Sprintf("%s set %s", query, strings.Join(updates, ", "))
		default:
		}

		if b.where != nil && b.queryMode != modeInsert {
			where, _, err := NewExpressionBuilder(b.where, exprBuilderHandler).Build(b.params)
			if err != nil {
				return "", nil, err
			}
			if where != "" {
				query = fmt.Sprintf("%s where %s", query, where)
			}
		}

		if b.window != "" {
			query = fmt.Sprintf("%s window %s", query, b.window)
		}

		if b.queryMode == modeSelect && len(b.group) > 0 {
			query = fmt.Sprintf("%s group by %s", query, strings.Join(b.group, ","))
		}

		if b.queryMode == modeSelect {
			if len(b.sort) == 0 {
				b.sort = []string{"id asc"}
			}
			query = fmt.Sprintf("%s order by %s", query, strings.Join(b.sort, ","))
		}

		if b.page != 0 {
			offset := b.page
			if b.count != 0 {
				offset *= b.count
			}
			query = fmt.Sprintf("%s offset %d", query, offset)
		}
		if b.count != 0 {
			query = fmt.Sprintf("%s limit %d", query, b.count)
		}

		if b.queryMode != modeSelect && b.conflictKey != "" {
			if len(b.conflictFields) == 0 {
				query = fmt.Sprintf("%s on conflict (%s) do nothing", query, b.conflictKey)
			} else {
				var conflicts []string
				for _, field := range b.conflictFields {
					conflicts = append(conflicts, fmt.Sprintf("%s=excluded.%s", field, field))
				}
				query = fmt.Sprintf("%s on conflict (%s) do update set %s", query, b.conflictKey, strings.Join(conflicts, ", "))
			}
		}

		if len(b.returning) > 0 && b.queryMode != modeSelect {
			query = fmt.Sprintf("%s returning %s", query, strings.Join(b.returning, ","))
		}

		return query, b.params, nil
	}
}
