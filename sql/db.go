package sql

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
)

var (
	ErrCannotConnectDb = fmt.Errorf("unable to connect to a db")
)

type Db interface {
	Connect() (Db, error)
	BeginTxx(ctx context.Context, opts *sql.TxOptions) (Tx, error)
	Database() *sql.DB
}

type Tx interface {
	SelectContext(ctx context.Context, dest any, query string, args ...any) error
	GetContext(ctx context.Context, dest any, query string, args ...any) error
	ExecContext(ctx context.Context, query string, args ...any) (sql.Result, error)
	NamedExecContext(ctx context.Context, query string, arg interface{}) (sql.Result, error)
	DriverName() string
	Commit() error
	Rollback() error
}

func NewDb(
	driver string,
	dsn string,
) *db {
	return &db{
		driver: driver,
		dsn:    dsn,
	}
}

type db struct {
	driver string
	dsn    string
	db     *sqlx.DB
}

func (c *db) Connect() (Db, error) {
	if c.db == nil || c.db.Ping() != nil {
		conn, err := sqlx.Connect(c.driver, c.dsn)
		if err != nil {
			return nil, fmt.Errorf("%w: %v", ErrCannotConnectDb, err)
		}
		c.db = conn
	}
	return c, nil
}

func (c *db) BeginTxx(ctx context.Context, opts *sql.TxOptions) (Tx, error) {
	return c.db.BeginTxx(ctx, opts)
}

func (c *db) Database() *sql.DB {
	return c.db.DB
}

func (c *db) Close() error {
	if c.db != nil {
		return c.db.Close()
	}
	return nil
}
