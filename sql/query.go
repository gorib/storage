package sql

import (
	"context"
	"fmt"
)

type selectHandler func(ctx context.Context, dest any, query string, args ...any) error

func Get[Result any](ctx context.Context, builder Builder, dest ...*Result) (Result, error) {
	tx, err := TransactionFromContext(ctx)
	if err != nil {
		return *new(Result), err
	}
	builder.withDialect(tx.DriverName())
	handler := tx.GetContext
	return query(ctx, handler, builder, dest...)
}

func Select[Result []E, E any](ctx context.Context, builder Builder, dest ...*Result) (Result, error) {
	tx, err := TransactionFromContext(ctx)
	if err != nil {
		return *new(Result), err
	}
	builder.withDialect(tx.DriverName())
	handler := tx.SelectContext
	return query(ctx, handler, builder, dest...)
}

func query[Result any](ctx context.Context, handler selectHandler, builder Builder, dest ...*Result) (result Result, err error) {
	var q string
	var args *[]any
	q, args, err = builder.Returning("*").Build()
	if err != nil {
		return result, err
	}

	err = handler(ctx, &result, q, *args...)
	if err != nil {
		return result, fmt.Errorf("%w (%s, %v)", err, q, *args)
	}
	if len(dest) == 1 {
		*dest[0] = result
	}
	return result, nil
}

func Exec(ctx context.Context, builder Builder) (_ int, err error) {
	tx, err := TransactionFromContext(ctx)
	if err != nil {
		return 0, err
	}
	builder.withDialect(tx.DriverName())
	handler := tx.ExecContext

	var q string
	var args *[]any
	q, args, err = builder.Build()
	if err != nil {
		return 0, err
	}

	result, err := handler(ctx, q, *args...)
	if err != nil {
		return 0, fmt.Errorf("%w (%s, %v)", err, q, *args)
	}
	var count int64
	count, err = result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(count), nil
}

func ExecNamed(ctx context.Context, builder Builder) (_ int, err error) {
	tx, err := TransactionFromContext(ctx)
	if err != nil {
		return 0, err
	}
	builder.withDialect(tx.DriverName())
	handler := tx.NamedExecContext

	var q string
	var args *[]any
	q, args, err = builder.named(true).Build()
	if err != nil {
		return 0, err
	}

	result, err := handler(ctx, q, *args)
	if err != nil {
		return 0, fmt.Errorf("%w (%s, %v)", err, q, *args)
	}
	var count int64
	count, err = result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(count), nil
}
