package sql

import (
	"fmt"
	"strings"

	"gitlab.com/gorib/criteria"
)

var (
	ErrExprBuilderNotImplemented = fmt.Errorf("expression builder is not defined")
	ErrUnsupportedQueryMode      = fmt.Errorf("unsupported query mode")
)

type exprBuilderFunc func(field string, value any, operation string, params *[]any) (string, *[]any, error)

func NewExpressionBuilder(e criteria.Expression, builder exprBuilderFunc) *exprBuilder {
	return &exprBuilder{builder: builder, e: e}
}

type exprBuilder struct {
	e       criteria.Expression
	builder exprBuilderFunc
}

func (e *exprBuilder) Build(args ...*[]any) (string, *[]any, error) {
	if e.builder == nil {
		return "", nil, ErrExprBuilderNotImplemented
	}
	if e == nil || e.e == nil {
		return "", nil, nil
	}
	expression, ok := e.e.(criteria.Builder)
	if !ok {
		return "", nil, fmt.Errorf("need a builder here")
	}
	strategy, err := expression.Strategy()
	if err != nil {
		return "", nil, err
	}
	parts, err := expression.Params()
	if err != nil {
		return "", nil, err
	}
	var params *[]any
	if len(args) > 0 {
		params = args[0]
	} else {
		params = &[]any{}
	}

	var query []string
	var q string
	for _, part := range parts {
		if ex, ok := part.(criteria.Expression); ok {
			q, params, err = NewExpressionBuilder(ex, e.builder).Build(params)
			if err != nil {
				return "", nil, err
			}
			if q != "" {
				query = append(query, q)
			}
		} else {
			v := part.([]any)
			field := v[0].(string)
			operation := v[1].(string)
			value := v[2]

			q, params, err = e.builder(field, value, operation, params)
			if err != nil {
				return "", nil, err
			}
			query = append(query, q)
		}
	}

	if strategy == "" {
		strategy = criteria.StrategyAnd
	}
	if strategy != criteria.StrategyAnd && strategy != criteria.StrategyOr {
		return "", nil, fmt.Errorf("%w: '%s'", ErrUnsupportedQueryMode, strategy)
	}
	if len(query) == 0 {
		return "", params, nil
	}
	return fmt.Sprintf("(%s)", strings.Join(query, fmt.Sprintf(") %s (", strategy))), params, nil
}
