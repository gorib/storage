package sql

import (
	"fmt"
	"reflect"

	"gitlab.com/gorib/criteria"
)

const (
	modeSelect uint8 = iota
	modeDelete
	modeInsert
	modeUpdate
)

var (
	ErrUnsupportedDriverForBuilder = fmt.Errorf("query builder doesn't support db driver")
)

type SqlExpr interface {
	String() string
	raw()
}

type raw struct {
	SqlExpr
	expression string
}

func (r *raw) String() string {
	return r.expression
}

func Raw(expression string) *raw {
	return &raw{expression: expression}
}

type Builder interface {
	SubTable(Builder) Builder
	Where(criteria.Expression) Builder
	Page(int, int) Builder
	Sort(sort ...string) Builder
	Window(window string) Builder
	Build() (string, *[]any, error)
	Returning(fields ...string) Builder
	Group(fields ...string) Builder
	NewCount(field string) Builder
	named(value bool) Builder
	parameters(*[]any) Builder
	withDialect(string) Builder
}

type InsertBuilder interface {
	Builder
	Values(...any) InsertBuilder
	Conflict(conflict string, excluded ...string) InsertBuilder
}

type TableBuilder interface {
	Builder
	Select(fields ...string) Builder
	Delete() Builder
	Insert(fields ...string) InsertBuilder
	Update(values map[string]any) Builder
}

func NewBuilder(table ...string) TableBuilder {
	b := &builder{}
	if len(table) > 0 {
		b.table = table[0]
	}
	return b
}

func (b *builder) Select(fields ...string) Builder {
	b.queryMode = modeSelect
	b.fields = fields
	return b
}

func (b *builder) Delete() Builder {
	b.queryMode = modeDelete
	return b
}

func (b *builder) Insert(fields ...string) InsertBuilder {
	b.queryMode = modeInsert
	b.fields = fields
	return b
}

func (b *builder) Update(values map[string]any) Builder {
	b.queryMode = modeUpdate
	b.updates = values
	return b
}

type builder struct {
	queryMode      uint8
	fields         []string
	table          string
	subSelect      Builder
	inserts        [][]any
	updates        map[string]any
	where          criteria.Expression
	window         string
	page           int
	count          int
	sort           []string
	conflictKey    string
	conflictFields []string
	returning      []string
	namedMode      bool
	params         *[]any
	group          []string
	dialect        string
	fail           error
}

func (b *builder) Group(fields ...string) Builder {
	b.group = fields
	return b
}

func (b *builder) Returning(fields ...string) Builder {
	b.returning = fields
	return b
}

func (b *builder) named(value bool) Builder {
	b.namedMode = value
	return b
}

func (b *builder) parameters(params *[]any) Builder {
	b.params = params
	return b
}

func (b *builder) withDialect(dialect string) Builder {
	b.dialect = dialect
	if b.subSelect != nil {
		b.subSelect.withDialect(dialect)
	}
	return b
}

func (b *builder) Where(e criteria.Expression) Builder {
	b.where = e
	return b
}

func (b *builder) NewCount(field string) Builder {
	if field == "" {
		field = "*"
	}
	return &builder{
		queryMode: modeSelect,
		fields:    []string{"count(" + field + ") as cnt"},
		table:     b.table,
		subSelect: b.subSelect,
		where:     b.where,
		window:    b.window,
		sort:      []string{"cnt"},
		returning: b.returning,
		params:    b.params,
		group:     b.group,
		dialect:   b.dialect,
		fail:      nil,
	}
}

func (b *builder) SubTable(subtable Builder) Builder {
	b.subSelect = subtable
	return b
}

func (b *builder) Page(page, count int) Builder {
	b.page = page
	b.count = count
	return b
}

func (b *builder) Sort(sort ...string) Builder {
	b.sort = sort
	return b
}

func (b *builder) Window(window string) Builder {
	b.window = window
	return b
}

func (b *builder) Build() (string, *[]any, error) {
	if b.fail != nil {
		return "", nil, b.fail
	}
	if b.table != "" && b.subSelect != nil {
		return "", nil, fmt.Errorf("cannot use both table and subquery")
	}
	if handler, ok := dialects[b.dialect]; ok {
		return handler(b)
	}
	return "", nil, fmt.Errorf("%w: '%s'", ErrUnsupportedDriverForBuilder, b.dialect)
}

func (b *builder) Values(values ...any) InsertBuilder {
	// 0 — uninitialized; 1 — [][]any; 2 — []any
	mode := 0
	var inserts [][]any
	for _, value := range values {
		if v := reflect.ValueOf(value); v.Type().Kind() == reflect.Slice {
			if mode != 0 && mode != 1 {
				b.fail = fmt.Errorf("cannot mix values, structs and slices")
			}
			mode = 1
			var item []any
			for i := 0; i < v.Len(); i++ {
				item = append(item, v.Index(i).Interface())
			}
			inserts = append(inserts, item)
		} else {
			if mode != 0 && mode != 2 {
				b.fail = fmt.Errorf("cannot mix values, structs and slices")
			}
			mode = 2
		}
	}
	if mode == 2 {
		inserts = append(inserts, values)
	}
	b.inserts = inserts
	return b
}

func (b *builder) Conflict(conflict string, excluded ...string) InsertBuilder {
	b.conflictKey = conflict
	b.conflictFields = excluded
	return b
}
