package sql

import (
	"context"
)

func NewRepository[Model any](connection Db) *repository[Model] {
	return &repository[Model]{DB: connection}
}

type repository[Model any] struct {
	DB Db
}

func (r *repository[Model]) Begin(ctx context.Context) (context.Context, func() error, func() error, error) {
	return NewContextWithTransaction(ctx, r.DB)
}

func (r *repository[Model]) Get(ctx context.Context, builder Builder) (model Model, err error) {
	ctx, commit, rollback, err := r.Begin(ctx)
	if err != nil {
		return *new(Model), err
	}
	defer func() {
		if e := rollback(); err == nil {
			err = e
		}
	}()
	model, err = Get[Model](ctx, builder)
	if err != nil {
		return *new(Model), err
	}
	err = commit()
	return
}

func (r *repository[Model]) Select(ctx context.Context, builder Builder) (model []*Model, err error) {
	ctx, commit, rollback, err := r.Begin(ctx)
	if err != nil {
		return nil, err
	}
	defer func() {
		if e := rollback(); err == nil {
			err = e
		}
	}()
	model, err = Select[[]*Model](ctx, builder)
	if err != nil {
		return nil, err
	}
	err = commit()
	return
}

func (r *repository[Model]) Delete(ctx context.Context, builder Builder) (err error) {
	ctx, commit, rollback, err := r.Begin(ctx)
	if err != nil {
		return err
	}
	defer func() {
		if e := rollback(); err == nil {
			err = e
		}
	}()
	_, err = Exec(ctx, builder)
	if err != nil {
		return err
	}
	return commit()
}

func (r *repository[Model]) Count(ctx context.Context, query Builder) (_ int, err error) {
	ctx, commit, rollback, err := r.Begin(ctx)
	if err != nil {
		return 0, err
	}
	defer func() {
		if e := rollback(); err == nil {
			err = e
		}
	}()

	var count []struct {
		Count int `db:"cnt"`
	}

	_, err = Select(ctx, query.NewCount(""), &count)
	if err != nil {
		return 0, err
	}
	err = commit()
	if err != nil {
		return 0, err
	}
	if len(count) == 0 {
		return 0, nil
	}
	return count[0].Count, nil
}
