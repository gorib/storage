package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"path/filepath"
	"regexp"
	"strings"
)

const (
	sqlWrapper = "c87d7b66130cd9245a1d3f5dde49683a"
)

type Lexem struct {
	Name    string
	Fields  []*LexemField
	Model   string
	Module  string
	Pointer string
	Imports []string
}

func (m *Lexem) addImport(module string) {
	for _, mod := range m.Imports {
		if module == mod {
			return
		}
	}
	m.Imports = append(m.Imports, module)
}

type LexemField struct {
	Name    string
	Source  string
	Field   string
	Wrapped bool
	Convert string
}

func parse(fileName string) (string, []*Lexem) {
	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, fileName, nil, parser.ParseComments)
	if err != nil {
		panic(err)
	}

	var packageName string
	var structs []*Lexem
	imports := map[string]string{}

	ast.Inspect(f, func(n ast.Node) bool {
		switch t := n.(type) {
		case *ast.File:
			packageName = t.Name.Name
		case *ast.ImportSpec:
			imp := strings.Trim(t.Path.Value, "\"")
			alias := filepath.Base(imp)
			if t.Name != nil {
				alias = t.Name.Name
			}
			imports[alias] = t.Path.Value
		case *ast.GenDecl:
			if t.Tok == token.TYPE {
				var model string
				var module string
				var pointer string
				if t.Doc != nil {
					for _, comment := range t.Doc.List {
						if matches := regexp.MustCompile("// sql.store ([*]?)(([a-zA-Z_]*).)?([a-zA-Z_]*)").FindStringSubmatch(comment.Text); len(matches) > 0 {
							if matches[1] == "*" {
								pointer = "*"
								matches = matches[2:]
							} else {
								matches = matches[1:]
							}
							if len(matches) == 3 {
								module = matches[1]
								model = matches[2]
							} else {
								model = matches[1]
							}
						}
					}
				}
				if model == "" {
					return true
				}

				typeSpec := t.Specs[0].(*ast.TypeSpec)
				structSpec := typeSpec.Type.(*ast.StructType)

				lexem := &Lexem{
					Name:    typeSpec.Name.Name,
					Model:   model,
					Module:  module,
					Pointer: pointer,
				}
				if module != "" {
					lexem.addImport(fmt.Sprintf("%s %s", module, imports[module]))
				}
				structs = append(structs, lexem)

				for _, field := range structSpec.Fields.List {
					if field.Tag == nil || field.Tag.Value == "" {
						continue
					}
					db := regexp.MustCompile("db:\"([^\"]*)\"").FindStringSubmatch(field.Tag.Value)
					store := regexp.MustCompile("store:\"([^\"]*)\"").FindStringSubmatch(field.Tag.Value)

					if len(db) != 2 || len(store) == 2 && store[1] == "-" {
						continue
					}
					dbTag := strings.Split(db[1], ",")[0]
					value := field.Names[0].Name
					var convert string
					if len(store) == 2 {
						storeTag := strings.Split(store[1], ",")
						if storeTag[0] != "" {
							value = storeTag[0]
						}
						for _, part := range storeTag {
							if part == "readonly" {
								dbTag = ""
							}
							if converts := strings.Split(part, ":"); len(converts) == 2 && converts[0] == "convert" {
								convert = converts[1]
								if convert == "csv" {
									lexem.addImport("\"strings\"")
								}
							}
						}
					}
					var wrapped bool
					if star, ok := field.Type.(*ast.StarExpr); ok {
						if idx, ok := star.X.(*ast.IndexExpr); ok {
							if sel, ok := idx.X.(*ast.SelectorExpr); ok {
								if sel.Sel.String() == "Json" {
									wrapped = true
									lexem.addImport(fmt.Sprintf("%s %s", sqlWrapper, imports[sel.X.(*ast.Ident).Name]))
								}
							}
						}
					}
					lexem.Fields = append(lexem.Fields, &LexemField{
						Name:    field.Names[0].Name,
						Source:  value,
						Field:   dbTag,
						Wrapped: wrapped,
						Convert: convert,
					})
				}
			}
		}
		return true
	})

	return packageName, structs
}
