package sql

import (
	"context"
	"fmt"
)

var (
	ErrNoTransactionInContext = fmt.Errorf("no started transaction in context")
	ErrCannotStartTx          = fmt.Errorf("unable to start a transaction")

	ErrCannotCreateSavepoint   = fmt.Errorf("unable to create a savepoint")
	ErrCannotReleaseSavepoint  = fmt.Errorf("unable to release a savepoint")
	ErrCannotRollbackSavepoint = fmt.Errorf("unable to rollback a savepoint")

	ErrCannotCommitTransaction   = fmt.Errorf("unable to commit a transaction")
	ErrCannotRollbackTransaction = fmt.Errorf("unable to rollback a transaction")
)

type transactionKey struct{}

type transaction struct {
	Tx
	level  int
	isDone bool
}

func NewContextWithTransaction(ctx context.Context, connection Db) (_ context.Context, commit func() error, rollback func() error, _ error) {
	if existsTx, ok := ctx.Value(transactionKey{}).(*transaction); ok && !existsTx.isDone {
		existsTx.level++
		isDone := false
		savepoint := fmt.Sprintf("level_%d", existsTx.level)
		if _, err := existsTx.ExecContext(ctx, fmt.Sprintf("savepoint %s", savepoint)); err != nil {
			return nil, nil, nil, fmt.Errorf("%w: %v", ErrCannotCreateSavepoint, err)
		}
		commit = func() error {
			if !isDone {
				if _, err := existsTx.ExecContext(ctx, fmt.Sprintf("release savepoint %s", savepoint)); err != nil {
					return fmt.Errorf("%w: %v", ErrCannotReleaseSavepoint, err)
				}
			}
			isDone = true
			return nil
		}
		rollback = func() error {
			if !isDone {
				if _, err := existsTx.ExecContext(ctx, fmt.Sprintf("rollback to savepoint %s", savepoint)); err != nil {
					return fmt.Errorf("%w: %v", ErrCannotRollbackSavepoint, err)
				}
			}
			isDone = true
			return nil
		}
	} else {
		conn, err := connection.Connect()
		if err != nil {
			return nil, nil, nil, err
		}
		if tx, err := conn.BeginTxx(ctx, nil); err != nil {
			return nil, nil, nil, fmt.Errorf("%w: %v", ErrCannotStartTx, err)
		} else {
			existsTx = &transaction{Tx: tx}
		}
		ctx = context.WithValue(ctx, transactionKey{}, existsTx)
		commit = func() error {
			if err := existsTx.Tx.Commit(); err != nil {
				return fmt.Errorf("%w: %v", ErrCannotCommitTransaction, err)
			}
			existsTx.isDone = true
			return nil
		}
		rollback = func() error {
			if !existsTx.isDone {
				if err := existsTx.Tx.Rollback(); err != nil {
					return fmt.Errorf("%w: %v", ErrCannotRollbackTransaction, err)
				}
			}
			existsTx.isDone = true
			return nil
		}
	}

	return ctx, commit, rollback, nil
}

func TransactionFromContext(ctx context.Context) (Tx, error) {
	if existsTx, ok := ctx.Value(transactionKey{}).(*transaction); ok && !existsTx.isDone {
		return existsTx.Tx, nil
	}
	return nil, ErrNoTransactionInContext
}
