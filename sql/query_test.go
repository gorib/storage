package sql

import (
	"context"
	"database/sql"
	"fmt"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"

	"gitlab.com/gorib/criteria"
)

type dbMock struct {
	*sqlx.DB
}

func (d *dbMock) Connect() (Db, error) {
	return d, nil
}

func (d *dbMock) BeginTxx(ctx context.Context, opts *sql.TxOptions) (Tx, error) {
	return d.DB.BeginTxx(ctx, opts)
}

func (d *dbMock) Database() *sql.DB {
	return d.DB.DB
}

func TestQuery(t *testing.T) {
	err := RegisterAlias("postgres", "sqlmock")
	assert.Nil(t, err)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(t, err)

	mock.ExpectBegin()
	repo := &dbMock{sqlx.NewDb(mockDB, "sqlmock")}
	ctx, _, _, err := NewContextWithTransaction(context.Background(), repo)
	assert.Nil(t, err)

	t.Run("Success", func(t *testing.T) {
		mock.ExpectQuery(regexp.QuoteMeta("select * from test where (f = $1) order by id asc")).WithArgs(17).WillReturnRows(sqlmock.NewRows([]string{"a"}).AddRow(1))
		result, err := Get[int](ctx, NewBuilder("test").Select("*").Where(criteria.And("f", "eq", 17)))
		assert.Nil(t, err)
		assert.Equal(t, 1, result)
	})

	t.Run("Insert", func(t *testing.T) {
		mock.ExpectQuery(regexp.QuoteMeta("insert into test (a, f) values ($1, $2) returning *")).WithArgs(1, 2).WillReturnRows(sqlmock.NewRows([]string{"a"}).AddRow(1))
		result, err := Get[int](ctx, NewBuilder("test").Insert("a", "f").Values(1, 2))
		assert.Nil(t, err)
		assert.Equal(t, 1, result)
	})

	t.Run("SuccessDestLink", func(t *testing.T) {
		mock.ExpectQuery(regexp.QuoteMeta("select * from test where (f = $1) order by id asc")).WithArgs(17).WillReturnRows(sqlmock.NewRows([]string{"a"}).AddRow(1))

		var link int
		result, err := Get[int](ctx, NewBuilder("test").Select("*").Where(criteria.And("f", "eq", 17)), &link)
		assert.Nil(t, err)
		assert.Equal(t, 1, result)
		assert.Equal(t, link, result)
	})

	t.Run("ErrorOnQuery", func(t *testing.T) {
		mock.ExpectQuery(regexp.QuoteMeta("select * from test where (f = $1) order by id asc")).WithArgs(17).WillReturnError(fmt.Errorf("something went wrong"))

		_, err = Get[int](ctx, NewBuilder("test").Select("*").Where(criteria.And("f", "eq", 17)))
		assert.ErrorContains(t, err, "something went wrong")
	})

	t.Run("QueryLike", func(t *testing.T) {
		mock.ExpectQuery(regexp.QuoteMeta("select * from test where (f like $1) order by id asc")).WithArgs("%tes").WillReturnError(fmt.Errorf("something went wrong"))

		_, err = Get[int](ctx, NewBuilder("test").Select("*").Where(criteria.And("f", "like", "%tes")))
		assert.ErrorContains(t, err, "something went wrong")
	})

	t.Run("Count", func(t *testing.T) {
		mock.ExpectQuery(regexp.QuoteMeta("select count(*) as cnt from test where (f = $1)")).WithArgs(17).WillReturnRows(sqlmock.NewRows([]string{"cnt"}).AddRow(1))
		cnt, err := Get(ctx, NewBuilder("test").Where(criteria.And("f", "eq", 17)).Sort("id").Page(1, 100).NewCount(""), new(struct {
			Cnt int `db:"cnt"`
		}))
		assert.Nil(t, err)
		assert.Equal(t, 1, cnt.Cnt)
	})
}

func TestExec(t *testing.T) {
	err := RegisterAlias("postgres", "sqlmock")
	assert.Nil(t, err)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(t, err)

	mock.ExpectBegin()
	repo := &dbMock{sqlx.NewDb(mockDB, "sqlmock")}
	ctx, _, _, err := NewContextWithTransaction(context.Background(), repo)
	assert.Nil(t, err)

	t.Run("Success", func(t *testing.T) {
		mock.ExpectExec(regexp.QuoteMeta("delete from test where (f = $1)")).WithArgs(17).WillReturnResult(sqlmock.NewResult(1, 2))
		result, err := Exec(ctx, NewBuilder("test").Delete().Where(criteria.And("f", "eq", 17)))
		assert.Nil(t, err)
		assert.Equal(t, 2, result)
	})

	t.Run("ErrorOnExec", func(t *testing.T) {
		mock.ExpectExec(regexp.QuoteMeta("select * from test where (f = $1) order by id asc")).WithArgs(17).WillReturnError(fmt.Errorf("something went wrong"))

		_, err = Exec(ctx, NewBuilder("test").Select("*").Where(criteria.And("f", "eq", 17)))
		assert.ErrorContains(t, err, "something went wrong")
	})
}

func TestExecNamed(t *testing.T) {
	err := RegisterAlias("postgres", "sqlmock")
	assert.Nil(t, err)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(t, err)

	mock.ExpectBegin()
	repo := &dbMock{sqlx.NewDb(mockDB, "sqlmock")}
	ctx, _, _, err := NewContextWithTransaction(context.Background(), repo)
	assert.Nil(t, err)

	t.Run("SuccessSingle", func(t *testing.T) {
		single := struct {
			Field int
		}{
			Field: 111,
		}

		mock.ExpectExec(regexp.QuoteMeta("insert into test (field) values (?)")).WithArgs(single.Field).WillReturnResult(sqlmock.NewResult(1, 2))
		result, err := ExecNamed(ctx, NewBuilder("test").Insert("field").Values(single))
		assert.Nil(t, err)
		assert.Equal(t, 2, result)
	})

	t.Run("SuccessMulti", func(t *testing.T) {
		list := []struct {
			Field int
		}{
			{
				Field: 111,
			},
			{
				Field: 222,
			},
		}

		mock.ExpectExec(regexp.QuoteMeta("insert into test (field) values (?)")).WithArgs(list[0].Field, list[1].Field).WillReturnResult(sqlmock.NewResult(1, 2))
		result, err := ExecNamed(ctx, NewBuilder("test").Insert("field").Values(list))
		assert.Nil(t, err)
		assert.Equal(t, 2, result)
	})

	t.Run("ErrorOnExec", func(t *testing.T) {
		list := []struct {
			Field int
		}{
			{
				Field: 111,
			},
			{
				Field: 222,
			},
		}
		mock.ExpectExec(regexp.QuoteMeta("insert into test (field) values (?)")).WithArgs(list[0].Field, list[1].Field).WillReturnError(fmt.Errorf("something went wrong"))

		_, err = ExecNamed(ctx, NewBuilder("test").Insert("field").Values(list))
		assert.ErrorContains(t, err, "something went wrong")
	})
}
